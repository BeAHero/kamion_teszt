import moment from 'moment';
import React, { useEffect, useState, useCallback } from 'react';
import Timeline from 'react-calendar-timeline';
import 'react-calendar-timeline/lib/Timeline.css';
import { v4 } from 'uuid';
import './App.css';

const App = () => {
  const bindTruckToOrder = (defaultTrucks, order) => {
    let handleTruck;
    defaultTrucks.forEach(truck => {
      if(truck.assignedOrderId.find((orderId) => orderId === order.id)) handleTruck = truck.name
    })
    return handleTruck
  }

  const [orderList, setOrderList] = useState([]);
  const restructureOrders = useCallback((orders, defaultTrucks) => {
    setOrderList(
      orders.map(order => {
        return {
          id: v4(),
          group: bindTruckToOrder(defaultTrucks, order),
          title: order.id,
          start_time: new Date(order.from),
          end_time: new Date(order.to)
        };
      })
    )
  }, []);

  const [truckList, setTruckList] = useState([]);
  const restructureTrucks = useCallback(trucks2 => {
    setTruckList(
      trucks2.map(truck => {
          return {
            id: truck.name,
            title: truck.name
          };
        })
      )
  }, []);

  const initData = useCallback(() =>{
    fetch('./trucktimeline.json'
    ,{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }).then(res => {
        return res.json();
      }).then(truckTimelineJson => {
        restructureTrucks(truckTimelineJson.trucks)
        restructureOrders(truckTimelineJson.orders, truckTimelineJson.trucks)
      });
  }, [restructureTrucks, restructureOrders]);

  useEffect(() => {
    initData()
  }, [initData])
 
  const [filteredTruckList, setFilteredTruckList] = useState()
  const filterTimeline = typedTRuckName => {
    setFilteredTruckList(truckList.filter(truck => truck.title.toLowerCase().includes(typedTRuckName.toLowerCase())))
  }

  return (
    <div className="truckAndOrderPage">
      <h1>Trucks and orders</h1>
      <label>
         Truck Filter: 
      </label>
      <input className="filterInput" type="text" onChange={e => filterTimeline(e.target.value)} />
      
      {
        truckList && truckList.length > 0 &&
        <Timeline
          groups={filteredTruckList && filteredTruckList.length ? filteredTruckList : truckList}
          items={orderList}
          defaultTimeStart={moment().add(-12, 'hour')}
          defaultTimeEnd={moment().add(12, 'hour')}
        />
      }
      
      <p>Let's ctrl scroll to zoom in!</p>
    </div>
  );
}

export default App;
